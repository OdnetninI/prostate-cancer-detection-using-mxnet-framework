import mxnet as mx
import logging
import sys
import cv2
import random
from functools import reduce
from sklearn.metrics import roc_curve, auc
from sklearn.metrics.ranking import roc_auc_score
import numpy as np
import matplotlib.pyplot as plt

def Conv(data, num_filter, kernel=(1, 1), stride=(1, 1), pad=(0, 0), name=None, suffix=''):
    conv = mx.sym.Convolution(data=data, num_filter=num_filter, kernel=kernel, stride=stride, pad=pad, no_bias=True, name='%s%s_conv2d' %(name, suffix))
    bn = mx.sym.BatchNorm(data=conv, name='%s%s_batchnorm' %(name, suffix), fix_gamma=True)
    act = mx.sym.Activation(data=bn, act_type='relu', name='%s%s_relu' %(name, suffix))
    return act


def Inception7A(data,
                num_1x1,
                num_3x3_red, num_3x3_1, num_3x3_2,
                num_5x5_red, num_5x5,
                pool, proj,
                name):
    tower_1x1 = Conv(data, num_1x1, name=('%s_conv' % name))
    tower_5x5 = Conv(data, num_5x5_red, name=('%s_tower' % name), suffix='_conv')
    tower_5x5 = Conv(tower_5x5, num_5x5, kernel=(5, 5), pad=(2, 2), name=('%s_tower' % name), suffix='_conv_1')
    tower_3x3 = Conv(data, num_3x3_red, name=('%s_tower_1' % name), suffix='_conv')
    tower_3x3 = Conv(tower_3x3, num_3x3_1, kernel=(3, 3), pad=(1, 1), name=('%s_tower_1' % name), suffix='_conv_1')
    tower_3x3 = Conv(tower_3x3, num_3x3_2, kernel=(3, 3), pad=(1, 1), name=('%s_tower_1' % name), suffix='_conv_2')
    pooling = mx.sym.Pooling(data=data, kernel=(3, 3), stride=(1, 1), pad=(1, 1), pool_type=pool, name=('%s_pool_%s_pool' % (pool, name)))
    cproj = Conv(pooling, proj, name=('%s_tower_2' %  name), suffix='_conv')
    concat = mx.sym.Concat(*[tower_1x1, tower_5x5, tower_3x3, cproj], name='ch_concat_%s_chconcat' % name)
    return concat

# First Downsample
def Inception7B(data,
                num_3x3,
                num_d3x3_red, num_d3x3_1, num_d3x3_2,
                pool,
                name):
    tower_3x3 = Conv(data, num_3x3, kernel=(3, 3), pad=(0, 0), stride=(2, 2), name=('%s_conv' % name))
    tower_d3x3 = Conv(data, num_d3x3_red, name=('%s_tower' % name), suffix='_conv')
    tower_d3x3 = Conv(tower_d3x3, num_d3x3_1, kernel=(3, 3), pad=(1, 1), stride=(1, 1), name=('%s_tower' % name), suffix='_conv_1')
    tower_d3x3 = Conv(tower_d3x3, num_d3x3_2, kernel=(3, 3), pad=(0, 0), stride=(2, 2), name=('%s_tower' % name), suffix='_conv_2')
    pooling = mx.sym.Pooling(data=data, kernel=(3, 3), stride=(2, 2), pad=(0,0), pool_type="max", name=('max_pool_%s_pool' % name))
    concat = mx.sym.Concat(*[tower_3x3, tower_d3x3, pooling], name='ch_concat_%s_chconcat' % name)
    return concat

def Inception7C(data,
                num_1x1,
                num_d7_red, num_d7_1, num_d7_2,
                num_q7_red, num_q7_1, num_q7_2, num_q7_3, num_q7_4,
                pool, proj,
                name):
    tower_1x1 = Conv(data=data, num_filter=num_1x1, kernel=(1, 1), name=('%s_conv' % name))
    tower_d7 = Conv(data=data, num_filter=num_d7_red, name=('%s_tower' % name), suffix='_conv')
    tower_d7 = Conv(data=tower_d7, num_filter=num_d7_1, kernel=(1, 7), pad=(0, 3), name=('%s_tower' % name), suffix='_conv_1')
    tower_d7 = Conv(data=tower_d7, num_filter=num_d7_2, kernel=(7, 1), pad=(3, 0), name=('%s_tower' % name), suffix='_conv_2')
    tower_q7 = Conv(data=data, num_filter=num_q7_red, name=('%s_tower_1' % name), suffix='_conv')
    tower_q7 = Conv(data=tower_q7, num_filter=num_q7_1, kernel=(7, 1), pad=(3, 0), name=('%s_tower_1' % name), suffix='_conv_1')
    tower_q7 = Conv(data=tower_q7, num_filter=num_q7_2, kernel=(1, 7), pad=(0, 3), name=('%s_tower_1' % name), suffix='_conv_2')
    tower_q7 = Conv(data=tower_q7, num_filter=num_q7_3, kernel=(7, 1), pad=(3, 0), name=('%s_tower_1' % name), suffix='_conv_3')
    tower_q7 = Conv(data=tower_q7, num_filter=num_q7_4, kernel=(1, 7), pad=(0, 3), name=('%s_tower_1' % name), suffix='_conv_4')
    pooling = mx.sym.Pooling(data=data, kernel=(3, 3), stride=(1, 1), pad=(1, 1), pool_type=pool, name=('%s_pool_%s_pool' % (pool, name)))
    cproj = Conv(data=pooling, num_filter=proj, kernel=(1, 1), name=('%s_tower_2' %  name), suffix='_conv')
    # concat
    concat = mx.sym.Concat(*[tower_1x1, tower_d7, tower_q7, cproj], name='ch_concat_%s_chconcat' % name)
    return concat

def Inception7D(data,
                num_3x3_red, num_3x3,
                num_d7_3x3_red, num_d7_1, num_d7_2, num_d7_3x3,
                pool,
                name):
    tower_3x3 = Conv(data=data, num_filter=num_3x3_red, name=('%s_tower' % name), suffix='_conv')
    tower_3x3 = Conv(data=tower_3x3, num_filter=num_3x3, kernel=(3, 3), pad=(0,0), stride=(2, 2), name=('%s_tower' % name), suffix='_conv_1')
    tower_d7_3x3 = Conv(data=data, num_filter=num_d7_3x3_red, name=('%s_tower_1' % name), suffix='_conv')
    tower_d7_3x3 = Conv(data=tower_d7_3x3, num_filter=num_d7_1, kernel=(1, 7), pad=(0, 3), name=('%s_tower_1' % name), suffix='_conv_1')
    tower_d7_3x3 = Conv(data=tower_d7_3x3, num_filter=num_d7_2, kernel=(7, 1), pad=(3, 0), name=('%s_tower_1' % name), suffix='_conv_2')
    tower_d7_3x3 = Conv(data=tower_d7_3x3, num_filter=num_d7_3x3, kernel=(3, 3), stride=(2, 2), name=('%s_tower_1' % name), suffix='_conv_3')
    pooling = mx.sym.Pooling(data=data, kernel=(3, 3), stride=(2, 2), pool_type=pool, name=('%s_pool_%s_pool' % (pool, name)))
    # concat
    concat = mx.sym.Concat(*[tower_3x3, tower_d7_3x3, pooling], name='ch_concat_%s_chconcat' % name)
    return concat

def Inception7E(data,
                num_1x1,
                num_d3_red, num_d3_1, num_d3_2,
                num_3x3_d3_red, num_3x3, num_3x3_d3_1, num_3x3_d3_2,
                pool, proj,
                name):
    tower_1x1 = Conv(data=data, num_filter=num_1x1, kernel=(1, 1), name=('%s_conv' % name))
    tower_d3 = Conv(data=data, num_filter=num_d3_red, name=('%s_tower' % name), suffix='_conv')
    tower_d3_a = Conv(data=tower_d3, num_filter=num_d3_1, kernel=(1, 3), pad=(0, 1), name=('%s_tower' % name), suffix='_mixed_conv')
    tower_d3_b = Conv(data=tower_d3, num_filter=num_d3_2, kernel=(3, 1), pad=(1, 0), name=('%s_tower' % name), suffix='_mixed_conv_1')
    tower_3x3_d3 = Conv(data=data, num_filter=num_3x3_d3_red, name=('%s_tower_1' % name), suffix='_conv')
    tower_3x3_d3 = Conv(data=tower_3x3_d3, num_filter=num_3x3, kernel=(3, 3), pad=(1, 1), name=('%s_tower_1' % name), suffix='_conv_1')
    tower_3x3_d3_a = Conv(data=tower_3x3_d3, num_filter=num_3x3_d3_1, kernel=(1, 3), pad=(0, 1), name=('%s_tower_1' % name), suffix='_mixed_conv')
    tower_3x3_d3_b = Conv(data=tower_3x3_d3, num_filter=num_3x3_d3_2, kernel=(3, 1), pad=(1, 0), name=('%s_tower_1' % name), suffix='_mixed_conv_1')
    pooling = mx.sym.Pooling(data=data, kernel=(3, 3), stride=(1, 1), pad=(1, 1), pool_type=pool, name=('%s_pool_%s_pool' % (pool, name)))
    cproj = Conv(data=pooling, num_filter=proj, kernel=(1, 1), name=('%s_tower_2' %  name), suffix='_conv')
    # concat
    concat = mx.sym.Concat(*[tower_1x1, tower_d3_a, tower_d3_b, tower_3x3_d3_a, tower_3x3_d3_b, cproj], name='ch_concat_%s_chconcat' % name)
    return concat

# In[49]:

def get_symbol(num_classes=1000, dtype='float32', **kwargs):
    data = mx.sym.Variable(name="data")
    if dtype == 'float32':
        data = mx.sym.identity(data=data, name='id')
    else:
        if dtype == 'float16':
            data = mx.sym.Cast(data=data, dtype=np.float16)

    # stage 1
    conv = Conv(data, 32, kernel=(3, 3), stride=(2, 2), name="conv")
    conv_1 = Conv(conv, 32, kernel=(3, 3), name="conv_1")
    conv_2 = Conv(conv_1, 64, kernel=(3, 3), pad=(1, 1), name="conv_2")
    pool = mx.sym.Pooling(data=conv_2, kernel=(3, 3), stride=(2, 2), pool_type="max", name="pool")
    # stage 2
    conv_3 = Conv(pool, 80, kernel=(1, 1), name="conv_3")
    conv_4 = Conv(conv_3, 192, kernel=(3, 3), name="conv_4")
    pool1 = mx.sym.Pooling(data=conv_4, kernel=(3, 3), stride=(2, 2), pool_type="max", name="pool1")
    # stage 3
    in3a = Inception7A(pool1, 64,
                       64, 96, 96,
                       48, 64,
                       "avg", 32, "mixed")
    in3b = Inception7A(in3a, 64,
                       64, 96, 96,
                       48, 64,
                       "avg", 64, "mixed_1")
    in3c = Inception7A(in3b, 64,
                       64, 96, 96,
                       48, 64,
                       "avg", 64, "mixed_2")
    in3d = Inception7B(in3c, 384,
                       64, 96, 96,
                       "max", "mixed_3")
    # stage 4
    in4a = Inception7C(in3d, 192,
                       128, 128, 192,
                       128, 128, 128, 128, 192,
                       "avg", 192, "mixed_4")
    in4b = Inception7C(in4a, 192,
                       160, 160, 192,
                       160, 160, 160, 160, 192,
                       "avg", 192, "mixed_5")
    in4c = Inception7C(in4b, 192,
                       160, 160, 192,
                       160, 160, 160, 160, 192,
                       "avg", 192, "mixed_6")
    in4d = Inception7C(in4c, 192,
                       192, 192, 192,
                       192, 192, 192, 192, 192,
                       "avg", 192, "mixed_7")
    in4e = Inception7D(in4d, 192, 320,
                       192, 192, 192, 192,
                       "max", "mixed_8")
    # stage 5
    in5a = Inception7E(in4e, 320,
                       384, 384, 384,
                       448, 384, 384, 384,
                       "avg", 192, "mixed_9")
    in5b = Inception7E(in5a, 320,
                       384, 384, 384,
                       448, 384, 384, 384,
                       "max", 192, "mixed_10")
    # pool
    pool = mx.sym.Pooling(data=in5b, kernel=(8, 8), stride=(1, 1), pool_type="avg", name="global_pool")
    flatten = mx.sym.Flatten(data=pool, name="flatten")
    fc1 = mx.sym.FullyConnected(data=flatten, num_hidden=num_classes, name='fc1')
    if dtype == 'float16':
        fc1 = mx.sym.Cast(data=fc1, dtype=np.float32)
    softmax = mx.sym.SoftmaxOutput(data=fc1, name='softmax')
    return softmax


def rotateImage(image, angle):
  image_center = tuple(np.array(image.shape[1::-1]) / 2)
  rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
  result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
  return result

def brightness(image, brightness_value):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    height, width, channels = hsv.shape	
    for x in range(height):
        for y in range(width):
            if hsv[x,y,2] + brightness_value > 255:
                hsv[x,y,2] = 255
            elif hsv[x,y,2] + brightness_value < 0:
                hsv[x,y,2] = 0
            else:
                hsv[x,y,2] += brightness_value
    edit_img = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return edit_img

def contrast(image, contrast_value):	
    height, width, channels = image.shape	
    for x in range(height):
        for y in range(width):
            for c in range(channels):
                if image[x,y,c] + contrast_value > 255:
                    image[x,y,c] = 255
                elif image[x,y,c] + contrast_value < 0:
                    image[x,y,c] = 0
                else:
                    image[x,y,c] += contrast_value
    return image

class DataAug(mx.io.DataIter):
    def __init__(self, iter):
        self.iter = iter
        self.imgs = 0
        
    def next(self):
        out = self.iter.next()
        data = out.data[0]
        
        for i in  range(0,len(data)):
            prob = random.randint(0,6)
            
            img = data[i].asnumpy()
            img = img.transpose((1, 2, 0))
            img = img.astype(np.uint8)
            
            #cv2.imwrite(str(self.imgs) + "_o_" + ".jpg", img)
            
            if prob == 1:
                img = rotateImage(img, 90)
            elif prob == 2:
                img = rotateImage(img, 180)
            elif prob == 3:
                img = rotateImage(img, 270)
            elif prob == 4:
                img = cv2.flip(img, 0)
            elif prob == 5:
                img = cv2.flip(img, 1)
            elif prob == 6:
                img = cv2.flip(img, -1)
            elif prob == 7: 
                img = contrast(img, 50)
            elif prob == 8:
                img = contrast(img, -50)
            elif prob == 9:
                img = brightness(img, 50)
            elif prob == 10:
                img = brightness(img, -50);
            
            #cv2.imwrite(str(self.imgs) + "_p_" + str(prob) + ".jpg", img)
            
            img = img.astype(np.float)
            img = img.transpose((2, 0, 1))
            data[i] = mx.nd.array(img)
            
            self.imgs += 1
        
        out.data[0] = data
        
        #if self.imgs == 16:
        #    exit(0)
        
        return out
        
    def reset(self):
        self.iter.reset()
        self.imgs = 0
        
    @property
    def provide_data(self):
        return self.iter.provide_data
    
    @property
    def provide_label(self):
        return self.iter.provide_label

# DEBUG
logger = logging.getLogger();
logger.setLevel(logging.DEBUG);
formatter = logging.Formatter('%(message)s');
h2 = logging.StreamHandler(sys.stdout)
h2.setFormatter(formatter);
logger.addHandler(h2)

mon = mx.mon.Monitor(1);
mx.random.seed(1);

# Define network
data = mx.symbol.Variable('data')
label = mx.sym.Variable('softmax_label')


net = data

net = mx.sym.Convolution(data = net, kernel=(3,3), num_filter=16)
net = mx.sym.Pooling(data = net, kernel=(2,2), pool_type="max")
net = mx.sym.Convolution(data = net, kernel=(5,5), num_filter=4)
net = mx.sym.Pooling(data = net, kernel=(2,2), pool_type="max")
net = mx.sym.Flatten(data = net)
net = mx.sym.FullyConnected(data = net, num_hidden = 500)
net = mx.sym.Activation(data = net, act_type = "sigmoid")
net = mx.sym.FullyConnected(data = net, num_hidden = 30)
net = mx.sym.Activation(data = net, act_type = "sigmoid")
net = mx.sym.FullyConnected(data = net, num_hidden = 2)
net = mx.sym.Activation(data = net, act_type = "sigmoid")
network = mx.symbol.SoftmaxOutput(data = net, label = label)
#network = get_symbol(num_classes=2)

# GPU Context
ctx = mx.gpu()
mx.random.seed(1,ctx);

# Read data
train = mx.image.ImageIter(batch_size = 16,
                            path_imglist = "/ssd/eduardo/database/original/list-clean-50-50-shuf.lst",
                            path_root = "/ssd/eduardo/database/original/",
                            data_shape=(3,128,128),
                            rand_crop = False,
                            rand_mirror = True,
                            shuffle = True)

val = mx.image.ImageIter(batch_size = 4,
                            path_imglist = "/ssd/eduardo/database/test/list-clean-50-50-shuf.lst",
                            path_root = "/ssd/eduardo/database/test/",
                            data_shape=(3,128,128),
                            rand_crop = False,
                            rand_mirror = False,
                            shuffle = True)

# Trainning Iterations
model = mx.mod.Module(symbol = network,
                      context = ctx,
                      data_names = ['data'],
                      label_names=['softmax_label'])

model.fit(train_data = train,
          eval_data = val,
          optimizer='sgd',
          optimizer_params={'learning_rate':0.001, 'momentum': 0.8},
          eval_metric=['acc','mcc','mse','ce'],
          initializer = mx.initializer.Xavier('uniform', 'avg', 1.0),
          batch_end_callback = mx.callback.Speedometer(32,64),
          #epoch_end_callback = mx.callback.do_checkpoint("model/model"),
	  #monitor=mon,
          num_epoch=15)

network.save("model/model-symbol.json");
model.save_params("model/model-0001.params");

# GET AUC

def get_labels(iterator, shape):
    iterator.reset()
    data = np.zeros(shape, dtype=np.float32)
    batch_size = iterator.batch_size
    c = 0
    for batch in iterator:
        label = batch.label
        data[c*batch_size:(c+1)*batch_size] = label[0][0].asnumpy()
        c += 1
    return data


val = mx.image.ImageIter(batch_size = 4,
                            path_imglist = "/ssd/eduardo/database/test/list-clean-50-50-shuf.lst",
                            path_root = "/ssd/eduardo/database/test/",
                            data_shape=(3,128,128),
                            rand_crop = False,
                            rand_mirror = False,
                            shuffle = False)

print("Started Predicting")
y_guess = model.predict(val)
print(y_guess.shape)

print("Get ground truth")
labels = get_labels(val, y_guess.shape)
print(labels.shape)

print("Calculate ROC")
roc = roc_auc_score(labels[:,0], y_guess.asnumpy()[:,1])
print(roc)

fpr = dict()
tpr = dict()
roc_auc = dict()
fpr[0], tpr[0], _ = roc_curve(labels[:,0], y_guess.asnumpy()[:,1])
roc_auc[0] = auc(fpr[0], tpr[0])

fig = plt.figure()
lw = 2
plt.plot(fpr[0], tpr[0], color='darkorange',
         lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[0])
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")

fig.savefig("plot.png")
fig.savefig("plot.svg")

print("Real labels")
print(labels[:,0])

print("Guess Labels")
print(labels[:,0])

# val = mx.image.ImageIter(batch_size = 1,
#                             path_imglist = "/ssd/eduardo/database/test/list-clean-025.lst",
#                             path_root = "/ssd/eduardo/database/test/",
#                             data_shape=(3,128,128),
#                             rand_crop = False,
#                             rand_mirror = False,
#                             shuffle = False)

# val.reset()
# num = 0
# good = 0
# for batch in val:
#     model.forward(batch)
#     prediction = model.get_outputs()
#     real = batch.label[0][0].asnumpy()[0]
#     tri = 0    
#     pred = prediction[0][0]
#     if (pred[0].asscalar() > pred[1].asscalar()):
#         tri = 0       
#     if (pred[1].asscalar() > pred[0].asscalar()):
#         tri = 1

#     if (tri == real):
#         good += 1
#     else:
#         print("Number " + str(num) + " is " + str(real) + " but neural says " + str(tri) + " with outputs ", end='')
#         print(pred.asnumpy())
        
#     num += 1

# acc = good/num

# print("Manual Validation Acc: " + str(acc))
