import mxnet as mx
import logging
import sys
from functools import reduce

# GPU Context
ctx = mx.gpu()

# Read data
val = mx.image.ImageIter(batch_size = 1,
                            path_imglist = "../database/test/322454/lista.lst",
                            path_root = "../database/test/322454/crop",
                            data_shape=(3,299,299),
                            rand_crop = False,
                            rand_mirror = False,
                            shuffle = False)

# Read Model
model = mx.mod.Module.load("model/model", 1, context=ctx)
model.bind(val.provide_data, val.provide_label, False);

# Interferance Iterations
val.reset()
num = 0
for batch in val:
    model.forward(batch)
    prediction = model.get_outputs()
    #tri = 0    
    pred = prediction[0][0]
    #if (pred[0].asscalar() > pred[1].asscalar()):# and pred[0].asscalar() > pred[2].asscalar()):
    #    tri = 0       
    #if (pred[1].asscalar() > pred[0].asscalar()): # and pred[1].asscalar() > pred[2].asscalar()):
    #    tri = 1
#    if (pred[2].asscalar() > pred[0].asscalar() and pred[2].asscalar() > pred[1].asscalar()):
 #       tri = 2

    print(str(num) + ":" + str(pred[1].asscalar()))
        
    num += 1
