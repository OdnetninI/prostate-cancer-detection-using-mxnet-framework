#!/bin/bash
#
#SBATCH -p watt-q
#SBATCH --workdir=/ssd/eduardo/neural/
#SBATCH -J "NEURAL Montage"
#SBATCH --cpus-per-task=12
#SBATCH --mail-type=ALL   # END/START/NONE
#SBATCH --mail-user=eduardojose.gomez@um.es

# Affinity
export KMP_AFFINITY=granularity=fine,compact

# Number of threads
export OMP_NUM_THREADS=12
#12

# Establecer la ruta relativa para el programa y argumentos,
# respecto al espacio de trabajo que hayas definido (workdir)
cd out
images=$(ls . | sort -n -t_ -k2 )
montage $images -tile 141x -geometry +0+0 ../result.jpg
