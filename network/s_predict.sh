#!/bin/bash
#
#SBATCH -p watt-q
#SBATCH --workdir=/ssd/eduardo/neural/
#SBATCH -J "NEURAL Predict"
#SBATCH --cpus-per-task=12
#SBATCH --mail-type=ALL   # END/START/NONE
#SBATCH --mail-user=eduardojose.gomez@um.es

# Affinity
export KMP_AFFINITY=granularity=fine,compact

# Number of threads
export OMP_NUM_THREADS=12
#12

# Establecer la ruta relativa para el programa y argumentos,
# respecto al espacio de trabajo que hayas definido (workdir)
export MXNET_CUDNN_AUTOTUNE_DEFAULT=0
module load cuda/9.2
module load python3/mxnet/gpu/1.3.0
python3.6 predict.py > predict.out 
