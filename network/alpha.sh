#!/bin/bash

list=$1
root=$2
out=$3

oldIFS=$IFS
IFS=$'\n'

for i in $(cat $1); do
    img=$(echo "$i" | cut -f 1 -d ":")
    col=$(echo "$i" | cut -f 2 -d ":")
    image="$root""/""crop_""$img"".jpg"
    python3.6 alpha.py $image $col "tmp.jpg"
    mv tmp.jpg "$out""/""crop_""$img"".jpg"
done

IFS=$oldIFS
