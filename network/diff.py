import cv2
import numpy as np
import sys

imagen = sys.argv[1]
color = sys.argv[2]

img1 = cv2.imread(imagen)
img2 = cv2.imread(color)
diff = img2 - img1

colorPixels = 0

# Every 128 Line of image
for line in img1:
    # Every Pixel inside a Line
    for pixel in line:
        # Check if image is white
        if ((pixel[0] < 215) or (pixel[1] < 215) or (pixel[2] < 215)):
            colorPixels = colorPixels + 1

# If more than 5% is non White
if colorPixels < 819:
    exit(0)
    
diff = cv2.subtract(img2, img1)
meanColor = [0,0,0]
colorPixels = 0

# Every 128 Line of image
for line,rline in zip(diff, img2):
    # Every Pixel inside a Line
    for pixel,rpixel in zip(line,rline):
        # Check if there any color
        if ((pixel[0] > 7) or (pixel[1] > 7) or (pixel[2] > 7)):
            meanColor[0] += rpixel[2];
            meanColor[1] += rpixel[1];
            meanColor[2] += rpixel[0];
            colorPixels += 1

if(colorPixels > 0):
    print(imagen, end=' ')
    meanColor[0] = meanColor[0]/colorPixels
    meanColor[1] = meanColor[1]/colorPixels
    meanColor[2] = meanColor[2]/colorPixels
    print(meanColor);


