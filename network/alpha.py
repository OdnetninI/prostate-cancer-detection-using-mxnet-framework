import cv2
import numpy as np
import sys

imagen = sys.argv[1]
color = float(sys.argv[2])
tmp = sys.argv[3]

color = color - 0.27880824
if color < 0:
    color = 0

img = cv2.imread(imagen)

#print(img.shape)

#if color == 0:
#    cv2.imwrite(tmp, img)
    
img = cv2.resize(img, (128,128))

mask = np.zeros((128,128,3), np.uint8)

dst = img

#if color == 1:
#    cv2.rectangle(mask, (0,0), (128,128), (0,0,255), -1)

#if color == 1:
#    cv2.rectangle(mask, (0,0), (128,128), (0,255,0), -1)

#if color != 0:
#    dst = cv2.addWeighted(img, 0.7, mask, 0.3, 0.0)
cv2.rectangle(mask, (0,0), (128,128), (0,255,0), -1)
dst = cv2.addWeighted(img, 1-color, mask, color, 0.0)
    
cv2.imwrite(tmp, dst)
