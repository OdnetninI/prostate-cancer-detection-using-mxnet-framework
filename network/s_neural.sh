#!/bin/bash
#
#SBATCH -p watt-q
#SBATCH --workdir=/ssd/eduardo/neural/
#SBATCH -J "NEURAL Network"
#SBATCH --cpus-per-task=12
#SBATCH --mail-type=END   # END/START/NONE
#SBATCH --mail-user=eduardojose.gomez@um.es

# Affinity
export KMP_AFFINITY=granularity=fine,compact

# Number of threads
export OMP_NUM_THREADS=12
#12

# Establecer la ruta relativa para el programa y argumentos,
# respecto al espacio de trabajo que hayas definido (workdir)
export MXNET_CPU_WORKER_NTHREADS=12
module load cuda/9.2
python3.6 neural.py
