# Working with the Neural Network

Compared to the generation of the database, this is easier to configure. Open each .sh and .py, and change the directories to yours. Most of them are only where are the databases, or the images itself.

In neural.py, in line 319, you have: "\#network = get_symbol(num_classes=2)" if you uncomment it, you work with inception, otherwise you will use our small network. Also, when using inception, you must specify tiles of 299x299, simple change any (3,128,128) to (3,299,299) on each file.

In s\_montage.sh there\'s the text: "-tile 141x", you have to change that 141 with the number of tiles in horizontal that you image have rounded up (ceil(image_width/128)).

When done, s\_neural.sh will train the network and generate the ROC curves with AUC. s\_predict.sh will generate an output file with the images and the probability of being cancerous. s\_alpha-parallel.sh will alpha blend all the tiles with the probabilities. And s\_montage.sh will recreate the image.
