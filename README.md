# First experiencies on Applying Deep Learning techniques to prostate cancer detection using MXNet framework

This is the repository with the code of the paper [](https://www.doi.org/)

Requirements:
 * python 3.6
 * mxnet >= 1.11
 * sk-learn
 * opencv2
 * cuda 9.2
 * ImageMagic
