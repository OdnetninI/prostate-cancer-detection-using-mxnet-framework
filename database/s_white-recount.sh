#!/bin/bash
#
#SBATCH -p watt-q
#SBATCH --workdir=/ssd/eduardo/database/original/
#SBATCH -J IMAGE-LIST
#SBATCH --cpus-per-task=12
#SBATCH --mail-type=ALL   # END/START/NONE
#SBATCH --mail-user=eduardojose.gomez@um.es

# Affinity
export KMP_AFFINITY=granularity=fine,compact

# Number of threads
export OMP_NUM_THREADS=12
#12
export LC_ALL=C

# Establecer la ruta relativa para el programa y argumentos,
# respecto al espacio de trabajo que hayas definido (workdir)

whiteRecount="whiteRecount.csv"

rm $whiteRecount

z="100 95 90 85 80 75 70 65 60 55 50 45 40 35 30 25 20 15 10 5 0"
num="0 1"

for i in $z; do 
	for j in $num; do 
		val=$(echo '_'$i'_'$j)
		eval $val=0
		
	done
done

u=$(cat whiteValues.csv | wc -l)
c=0

while read l ; do
echo "[$c/$u]"
c=$(($c + 1))
e=${l#*,}
d=${e%*,*}
w=${l##*,}
w=$(sed -E 's/([+-]?[0-9.]+)[eE]\+?(-?)([0-9]+)/(\1*10^\2\3)/g' <<< "$w")
for k in $z; do
o=$(echo $w'>='$k'/100' | bc -l)
v='_'$k'_'$d
n=$(eval echo '$'$v)
test "$o" == "0"
eval $v=$(( $n + 1 - $? ))
done
done < "whiteValues.csv"


for i in $z; do 
	echo -n "$i" >> $whiteRecount
	for j in $num; do 
		val=$(echo '_'$i'_'$j) 
		eval echo -n ',$'$val >> $whiteRecount
	done
	echo >> $whiteRecount
done
		
