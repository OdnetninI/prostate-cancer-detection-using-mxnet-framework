#!/bin/bash
#
#SBATCH -p watt-q
#SBATCH --workdir=/ssd/eduardo/database/original
#SBATCH -J IMAGE-DIFFERENCE
#SBATCH --cpus-per-task=12
#SBATCH --mail-type=ALL   # END/START/NONE
#SBATCH --mail-user=eduardojose.gomez@um.es

# Affinity
export KMP_AFFINITY=granularity=fine,compact

# Number of threads
export OMP_NUM_THREADS=12
#12

# Establecer la ruta relativa para el programa y argumentos,
# respecto al espacio de trabajo que hayas definido (workdir)
./diff.sh 322450/list 322450/crop/ 322450/crop-col/ 322450/diff
./diff.sh 322453/list 322453/crop/ 322453/crop-col/ 322453/diff
./diff.sh 322455/list 322455/crop/ 322455/crop-col/ 322455/diff
./diff.sh 315964/list 315964/crop/ 315964/crop-col/ 315964/diff
./diff.sh 315966/list 315966/crop/ 315966/crop-col/ 315966/diff
./diff.sh 315968/list 315968/crop/ 315968/crop-col/ 315968/diff
./diff.sh 315969/list 315969/crop/ 315969/crop-col/ 315969/diff


