#!/bin/bash
#
#SBATCH -p watt-q
#SBATCH --workdir=/ssd/eduardo/database/original/
#SBATCH -J IMAGE-Convert
#SBATCH --cpus-per-task=12
#SBATCH --mail-type=ALL   # END/START/NONE
#SBATCH --mail-user=eduardojose.gomez@um.es

# Affinity
export KMP_AFFINITY=granularity=fine,compact

# Number of threads
export OMP_NUM_THREADS=12
#12

# Establecer la ruta relativa para el programa y argumentos,
# respecto al espacio de trabajo que hayas definido (workdir)
convert 322444/322444.tif 322444/322444.jpg &
convert 322444/322444-col.tif 322444/322444-col.jpg &

convert 322446/322446-col.tif 322446/322446-col.jpg &
convert 322447/322447-col.tif 322447/322447-col.jpg &
convert 322449/322449-col.tif 322449/322449-col.jpg &
convert 322450/322450-col.tif 322450/322450-col.jpg &
convert 322453/322453-col.tif 322453/322453-col.jpg &
convert 322454/322454-col.tif 322454/322454-col.jpg &
convert 322455/322455-col.tif 322455/322455-col.jpg &
convert 315960/315960-col.tif 315960/315960-col.jpg &
convert 315963/315963-col.tif 315963/315963-col.jpg &
convert 315964/315964-col.tif 315964/315964-col.jpg &
convert 315965/315965-col.tif 315965/315965-col.jpg &
convert 315966/315966-col.tif 315966/315966-col.jpg &
convert 315968/315968-col.tif 315968/315968-col.jpg &
convert 315969/315969-col.tif 315969/315969-col.jpg &

wait %1 %2 %3 %4 %5 %6 %7 %8 %9 %10 %11 %12 %13 %14 %15 %16

