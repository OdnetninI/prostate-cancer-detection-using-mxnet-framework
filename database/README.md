# Database generation

All scripts are prepared to be used in Slurm workload system, but they can be run on local.

Make a folder for each image, with at least directories: "crop", "crop-col", and "difference".

When using JPG images, put each one into a folder (the ones we created before). Then change the script s\_cropping.sh to point to that image.

If you are using other image formats, see s\_jpg.sh to see how to convert them.

Then make a list of the crops inside each image directory: $(ls > list). When done, modify s\_diff.sh to your image folders. The script will generate a diff file with tile numbers and colors. Move these tiles from "crop-col" to "difference".

Now you need to create a list.lst with the labels. You can start making a csv file per image. The first collumn is the filename, and the second is the label. Any image which remains in "crop-col" is labeled as 0, and the "differece" folder as 1 (Make your own script).

All csv must be named as "lista.csv". Then, s\_grab.sh will get all this files and combine them to create a list.lst.

Now you can run s\_white2.sh to filter any image above 95\% background (in this case white).

If you want to check how is you database (quantity of tiles after removing x% of white), you can run s\_white-validator.sh. (If you already have a csv with the percentage of white, you can usar s\_white-recount.sh, and optimized version of the aforementioned).

If all was right, you will have a list-095.lst with the tiles filtered. Now you can see how many tiles with cancer you have (grep -P "\t1\t" | wc -l) and how many healthy. Then, you can change the percentages like you want, for example 50-50.

When you have finished, you have your database ready. You can make the same (in another folder) for the test dataset.
