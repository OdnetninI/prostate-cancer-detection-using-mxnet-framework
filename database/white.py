import cv2
import numpy as np
import sys

imagen = sys.argv[1]

img = cv2.imread(imagen)

colorPixels = 0

# Every 128 Line of image
for line in img:
    # Every Pixel inside a Line
    for pixel in line:
        # Check if image is white
        if ((pixel[0] < 215) or (pixel[1] < 215) or (pixel[2] < 215)):
            colorPixels = colorPixels + 1

        # If more than 5% is non White
        if colorPixels > 819:
            print("0")
            exit(0)

print("1")
