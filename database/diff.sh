#!/bin/bash

list="$(cat $1)"
dir1=$2
dir2=$3
out=$4

parallel "python3.6 diff.py $dir1/{1} $dir2/{1}" < $1 >> $out

#for i in $list; do
#    python3.6 diff.py "$dir1"/"$i" "$dir2"/"$i" >> diffList
#done
