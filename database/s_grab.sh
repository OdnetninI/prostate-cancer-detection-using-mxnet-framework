#!/bin/bash
#
#SBATCH -p watt-q
#SBATCH --workdir=/ssd/eduardo/database/original/
#SBATCH -J IMAGE-LIST
#SBATCH --cpus-per-task=12
#SBATCH --mail-type=ALL   # END/START/NONE
#SBATCH --mail-user=eduardojose.gomez@um.es

# Affinity
export KMP_AFFINITY=granularity=fine,compact

# Number of threads
export OMP_NUM_THREADS=12
#12

# Establecer la ruta relativa para el programa y argumentos,
# respecto al espacio de trabajo que hayas definido (workdir)

csv="list.csv"
lst="list.lst"

rm $csv $lst

count="0"
a=$(ls -d */)
for j in $a; do

	for i in $(cat "$j""lista.csv"); do
		image=$(echo $i | cut -f 1 -d ",")
		data=$(echo $i | cut -f 2 -d ",")
		if [ "$data" -gt "1" ]; then
			data="1"
		fi		
		echo "$j""crop/""$image"",""$data" >> $csv
		echo -e "$count""\t""$data""\t""$j""crop/""$image"	>> $lst
		count=$(( $count + 1 ))
	done;

done
