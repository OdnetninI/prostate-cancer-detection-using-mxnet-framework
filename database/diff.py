import cv2
import numpy as np
import sys

imagen = sys.argv[1]
color = sys.argv[2]

img1 = cv2.imread(imagen)
img2 = cv2.imread(color)
diff = img2 - img1

colorPixels = 0

# Every 128 Line of image
for line in img1:
    # Every Pixel inside a Line
    for pixel in line:
        # Check if image is white
        if ((pixel[0] < 215) or (pixel[1] < 215) or (pixel[2] < 215)):
            colorPixels = colorPixels + 1

# If more than 5% is non White
if colorPixels < 819:
    exit(0)

diff = cv2.subtract(img2, img1)
meanColor = [0,0,0]
colorPixels = 0

t,diff = cv2.threshold(cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY), 15,255,cv2.THRESH_BINARY)

th = 0
# Every 128 Line of image
for line,rline in zip(diff, img2):
    # Every Pixel inside a Line
    for pixel,rpixel in zip(line,rline):
        # Check if there any color
        if (pixel > th):# or (pixel[1] > th) or (pixel[2] > th)):
            #pixel[0] = rpixel[0]
            #pixel[1] = rpixel[1]
            #pixel[2] = rpixel[2]
            meanColor[0] += rpixel[2];
            meanColor[1] += rpixel[1];
            meanColor[2] += rpixel[0];
            colorPixels += 1
        else:
            rpixel[0] = 0
            rpixel[1] = 0
            rpixel[2] = 0

if(colorPixels > 0):
    meanColor[0] = meanColor[0]/colorPixels
    meanColor[1] = meanColor[1]/colorPixels
    meanColor[2] = meanColor[2]/colorPixels
    print(imagen, end=' ')
    print(meanColor);
