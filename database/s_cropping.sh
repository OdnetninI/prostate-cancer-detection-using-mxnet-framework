#!/bin/bash
#
#SBATCH -p watt-q
#SBATCH --workdir=/ssd/eduardo/database/original/
#SBATCH -J IMAGE-CROPPING
#SBATCH --cpus-per-task=12
#SBATCH --mail-type=ALL   # END/START/NONE
#SBATCH --mail-user=eduardojose.gomez@um.es

# Affinity
export KMP_AFFINITY=granularity=fine,compact

# Number of threads
export OMP_NUM_THREADS=12
#12

# Image 1
cd 322446
mkdir crop 
convert 322446.jpg -crop 128x128 crop/crop_%d.jpg &
convert 322446-col.jpg -crop 128x128 crop-col/crop_%d.jpg &
cd ..

# Image 2
cd 322449
mkdir crop 
convert 322449.jpg -crop 128x128 crop/crop_%d.jpg &
convert 322449-col.jpg -crop 128x128 crop-col/crop_%d.jpg &
cd ..

wait %1 %2 %3 %4 # so on ...
