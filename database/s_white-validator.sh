#!/bin/bash
#
#SBATCH -p watt-q
#SBATCH --workdir=/ssd/eduardo/database/original/
#SBATCH -J IMAGE-LIST
#SBATCH --cpus-per-task=12
#SBATCH --mail-type=ALL   # END/START/NONE
#SBATCH --mail-user=eduardojose.gomez@um.es

# Affinity
export KMP_AFFINITY=granularity=fine,compact

# Number of threads
export OMP_NUM_THREADS=12
#12

# Establecer la ruta relativa para el programa y argumentos,
# respecto al espacio de trabajo que hayas definido (workdir)

whiteValues="whiteValues.csv"
whiteRecount="whiteRecount.csv"

rm $whiteValues $whiteRecount

values="100 95 90 85 80 75 70 65 60 55 50 45 40 35 30 25 20 15 10 5 0"
num="0 1"

for i in $values; do 
	for j in $num; do 
		val=$(echo '_'$i'_'$j)
		eval $val=0
		
	done
done


a=$(ls -d */)
for j in $a; do
	for i in $(cat "$j""lista.csv"); do
		image=$(echo $i | cut -f 1 -d ",")
		data=$(echo $i | cut -f 2 -d ",")
		if [ "$data" -gt "1" ]; then
			data="1"
		fi
		w=$(python3 white-value.py "$j""crop/""$image")
		echo "$j""crop/""$image"",""$data"",""$w" >> $whiteValues
		
		for k in $values; do
			out=$(echo $w'>='$(echo $k'/'100 | bc -l) | bc -l)
			if [ "$out" == "0" ]; then
				val=$(echo '_'$j'_'$data)
				
				n=$(eval echo '$'$val)
				export $val=$(( $n + 1 ))
			fi
		done
	done;
done


for i in $values; do 
	echo -n "$i" >> $whiteRecount
	for j in $num; do 
		val=$(echo '_'$i'_'$j)
		n=$(eval echo '$'$val)
		echo -n ",$n" >> $whiteRecount
	done
	echo >> $whiteRecount
done
		
